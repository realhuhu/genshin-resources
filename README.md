## 安装

进入py-plugin plugins目录，输入

```shell
git clone https://gitee.com/realhuhu/genshin-resources.git
```

在```config.yaml``` 的plugins下添加```genshin-resources```

重启即可

## 指令  

```#地图资源更新```  强制更新地图资源 

```#xxx在xxx哪```  如#甜甜花在哪、#薄荷在渊下宫哪。多个物品用空格分隔，如#甜甜花 薄荷在哪。默认地图为提瓦特大地图  

## 配置

```resource_alias ```物品别名，如下图

![image-20221231144143447](https://typora-1304907527.cos.ap-nanjing.myqcloud.com/202212311441475.png)

```icon_size ```物品图标大小，默认1

```transport_size``` 传送点图标大小，默认1

```genshin_resource_data_path``` 地图图标存放路径
