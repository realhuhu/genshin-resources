from pathlib import Path
from typing import List, Dict, Union

from pydantic import BaseModel, Extra

from nonebot import get_driver


class Config(BaseModel, extra=Extra.ignore):
    resource_alias = {}
    icon_size: float = 1
    transport_size: float = 1
    genshin_resource_data_path: Union[Path, str] = Path() / "data" / "genshin-resources"
    map_list_url: str = "https://api-takumi.mihoyo.com/common/map_user/ys_obc/v1/map/list?app_sn=ys_obc"
    map_info_url: str = "https://waf-api-takumi.mihoyo.com/common/map_user/ys_obc/v1/map/info?map_id=%s&app_sn=ys_obc&lang=zh-cn"
    icon_list_url: str = "https://waf-api-takumi.mihoyo.com/common/map_user/ys_obc/v1/map/label/tree?map_id=2&app_sn=ys_obc&lang=zh-cn"
    point_list_url: str = 'https://waf-api-takumi.mihoyo.com/common/map_user/ys_obc/v1/map/point/list?map_id=%s&app_sn=ys_obc&lang=zh-cn'


genshin_resource_config = Config.parse_obj(get_driver().config.dict())
